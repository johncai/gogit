package tree

import (
	"bytes"
	"crypto/sha1"
	"fmt"
	"io"
	"os"
	"path/filepath"
	"sort"
	"strconv"
	"strings"
	"time"

	"gitlab.com/johncai/gogit/repository/types"
)

type blobEntry struct {
	n string
	o types.Oid
	s os.FileInfo
}

func NewEntry(name string, oid [20]byte, stat os.FileInfo) *blobEntry {
	return &blobEntry{
		n: name,
		o: oid,
		s: stat,
	}
}

func (e *blobEntry) String() string {
	return e.name()
}

func (e *blobEntry) name() string {
	return e.n
}

func (e *blobEntry) oid() types.Oid {
	return e.o
}

func (e *blobEntry) mode() string {
	if e.s.Mode()&0111 != 0 {
		return "100755"
	}
	return "100644"
}

func (e blobEntry) isTree() bool {
	return false
}

func Build(entries []types.Entry) (*Tree, error) {
	sort.Sort(byFilename(entries))

	t := New()

	for _, e := range entries {
		dir, base := filepath.Split(e.Name())
		var dirs []string
		if dir != "" {
			dirs = strings.Split(strings.Trim(dir, `/`), `/`)
		}
		t.AddEntry(dirs, base, e)
	}
	return t, nil
}

func (t *Tree) AddEntry(dirs []string, name string, entry types.Entry) error {
	if len(dirs) == 0 {
		t.entries[name] = entry
		t.sortedEntryKeys = append(t.sortedEntryKeys, name)
		return nil
	}
	var tree *Tree
	if v, ok := t.entries[dirs[0]]; ok {
		tree = v.(*Tree)
	} else {
		tree = New()
		t.entries[dirs[0]] = tree
		t.sortedEntryKeys = append(t.sortedEntryKeys, dirs[0])
	}
	tree.AddEntry(dirs[1:], name, entry)
	tree.path = dirs[0]

	return nil
}

/*
func (t *Tree) Store(d *database.Database) error {
	for _, entry := range t.entries {
		if tree, ok := entry.(*Tree); ok {
			if err := tree.Store(d); err != nil {
				return err
			}
		}
	}
	t.o = sha1.Sum([]byte(t.String()))
	return d.Store(t)
}
*/

type Tree struct {
	path            string
	o               types.Oid
	entries         map[string]types.Entry
	sortedEntryKeys []string
}

func (t *Tree) SetOid(o types.Oid) {
	t.o = o
}

func (t *Tree) Entries() []types.Entry {
	var entries []types.Entry
	for _, key := range t.sortedEntryKeys {
		v, ok := t.entries[key]
		if ok {
			entries = append(entries, v)
		}
	}

	return entries
}

func New() *Tree {
	return &Tree{
		entries: make(map[string]types.Entry),
	}
}

type byFilename []types.Entry

func (b byFilename) Len() int {
	return len(b)
}

func (b byFilename) Swap(i, j int) {
	b[i], b[j] = b[j], b[i]
}

func (b byFilename) Less(i, j int) bool {
	return b[i].Name() < b[j].Name()
}

func (t *Tree) Type() string {
	return "tree"
}

func (t *Tree) Mode() os.FileMode {
	return os.FileMode(040000)
}

func (t *Tree) Oid() types.Oid {
	return t.o
}

func (t *Tree) Name() string {
	return t.path
}

func (t *Tree) IsDir() bool {
	return true
}

// ModTime returns dummy data to conform to os.FileInfo
func (t *Tree) ModTime() time.Time {
	return time.Time{}
}

// Size returns dummy data to conform to os.FileInfo
func (t *Tree) Size() int64 {
	return 0
}

// Sys returns dummy data to conform to os.FileInfo
func (t *Tree) Sys() interface{} {
	return nil
}

func (t *Tree) String() string {
	var rawEntries [][]byte

	for _, key := range t.sortedEntryKeys {
		entry, ok := t.entries[key]
		if !ok {
			panic("well i couldn't find it")
		}
		oid := entry.Oid()

		rawEntries = append(rawEntries, append([]byte(fmt.Sprintf("%06o %s\x00", entry.Mode(), entry.Name())), oid[:20]...))
	}

	contents := string(bytes.Join(rawEntries, []byte("")))

	return fmt.Sprintf("tree %d\x00%s", len(contents), contents)
}

func Parse(d []byte) (*Tree, error) {
	b := bytes.NewBuffer(d)

	entriesMap := make(map[string]types.Entry)
	entriesKey := make([]string, 0)

	for {
		entryBytes, err := b.ReadBytes(' ')
		if err != nil {
			if err == io.EOF {
				break
			}
			return nil, err
		}
		var entry ParsedEntry

		entryBytes = entryBytes[:len(entryBytes)-1]
		fileMode, err := strconv.ParseUint(string(entryBytes), 8, 32)
		if err != nil {
			return nil, err
		}
		entry.mode = os.FileMode(uint32(fileMode))

		entryBytes, err = b.ReadBytes(0x00)
		if err != nil {
			return nil, err
		}

		entryBytes = entryBytes[:len(entryBytes)-1]
		entry.name = string(entryBytes)

		var sha [sha1.Size]byte
		if _, err = b.Read(sha[:]); err != nil {
			return nil, err
		}
		entry.oid = sha

		entriesMap[entry.name] = &entry
		entriesKey = append(entriesKey, entry.name)
	}

	t := New()

	sort.Strings(entriesKey)
	t.entries = entriesMap
	t.sortedEntryKeys = entriesKey

	return t, nil
}

type ParsedEntry struct {
	oid  types.Oid
	t    string
	mode os.FileMode
	name string
}

func NewParsedEntry(oid types.Oid, t string, mode os.FileMode, name string) *ParsedEntry {
	return &ParsedEntry{
		oid:  oid,
		t:    t,
		mode: mode,
		name: name,
	}
}

func (p *ParsedEntry) Oid() types.Oid {
	return p.oid
}

func (p *ParsedEntry) String() string {
	return ""
}

func (p *ParsedEntry) Name() string {
	return p.name
}

func (p *ParsedEntry) Size() int64 {
	return 0
}

func (p *ParsedEntry) Mode() os.FileMode {
	return p.mode
}

func (p *ParsedEntry) ModTime() time.Time {
	return time.Time{}
}

func (p *ParsedEntry) IsDir() bool {
	return false
}

func (p *ParsedEntry) Sys() interface{} {
	return nil
}
