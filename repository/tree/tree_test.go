package tree_test

import (
	"bytes"
	"crypto/sha1"
	"io"
	"strconv"
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/johncai/gogit/repository/tree"
	"gitlab.com/johncai/gogit/repository/types"
)

func TestParseTree(t *testing.T) {
	entries := []types.Entry{
		tree.NewParsedEntry(
			[sha1.Size]byte{0x45, 0x58, 0x5c, 0x2a, 0xd9, 0x6a, 0xa2, 0x5f, 0xd2, 0x54, 0x4f, 0x13, 0x6d, 0x76, 0xe7, 0xe6, 0x6a, 0xe9, 0x77, 0x8e},
			"blob",
			16384,
			"lib/util.go",
		),
		tree.NewParsedEntry(
			[sha1.Size]byte{0x01, 0x67, 0xae, 0x8f, 0xd9, 0x6a, 0xa2, 0x5f, 0xd2, 0x54, 0x4f, 0x13, 0x6d, 0x76, 0xe7, 0xe6, 0x6a, 0xe9, 0x77, 0x8e},
			"blob",
			33188,
			"README.md",
		),
		tree.NewParsedEntry(
			[sha1.Size]byte{0x99, 0xa2, 0xb4, 0x2a, 0xd9, 0x6a, 0xa2, 0x5f, 0xd2, 0x54, 0x4f, 0x13, 0x6d, 0x76, 0xe7, 0xe6, 0x6a, 0xe9, 0x77, 0x8e},
			"blob",
			33188,
			"main.go",
		),
		tree.NewParsedEntry(
			[sha1.Size]byte{0x45, 0x58, 0x5c, 0x2a, 0xd9, 0x6a, 0xa2, 0x5f, 0xd2, 0x54, 0x4f, 0x13, 0x6d, 0x76, 0xe7, 0xe6, 0x6a, 0xe9, 0x77, 0x8e},
			"blob",
			33188,
			"config.toml",
		),
	}

	originalTree, err := tree.Build(entries)
	require.NoError(t, err)

	originalTreeData := originalTree.String()

	stringReader := bytes.NewBufferString(originalTreeData)

	_, err = stringReader.ReadBytes(' ')
	require.NoError(t, err)

	// read size
	sizeBytes, err := stringReader.ReadBytes(0x00)
	require.NoError(t, err)

	size, err := strconv.ParseInt(string(sizeBytes[:len(sizeBytes)-1]), 10, 64)
	require.NoError(t, err)

	var data bytes.Buffer

	_, err = io.Copy(&data, stringReader)
	require.NoError(t, err)

	parsedTree, err := tree.Parse(data.Bytes()[:size])
	require.NoError(t, err)

	require.Equal(t, originalTreeData, parsedTree.String())
}
