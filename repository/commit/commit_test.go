package commit_test

import (
	"bytes"
	"io"
	"strconv"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/johncai/gogit/repository/commit"
	"gitlab.com/johncai/gogit/repository/types"
)

func TestParseCommit(t *testing.T) {
	parent, err := types.ParseOid("6c0183a778d1948ae1a45cd78be4f2d7fea29bfb")
	require.NoError(t, err)
	tree, err := types.ParseOid("1d8af8a778d1948ae1a45cd78be4f2d7fea29bfb")
	require.NoError(t, err)

	author := commit.Author{
		Name:  "John Doe",
		Email: "jdoe@doe.com",
		Now:   time.Now(),
	}

	message := "this is a commit message"

	originalCommit := commit.New(parent, tree, &author, message)

	commitData := originalCommit.String()

	stringReader := bytes.NewBufferString(commitData)

	_, err = stringReader.ReadBytes(' ')
	require.NoError(t, err)

	// read size
	sizeBytes, err := stringReader.ReadBytes(0x00)
	require.NoError(t, err)

	size, err := strconv.ParseInt(string(sizeBytes[:len(sizeBytes)-1]), 10, 64)
	require.NoError(t, err)

	var data bytes.Buffer

	_, err = io.Copy(&data, stringReader)
	require.NoError(t, err)

	parsedCommit, err := commit.Parse(data.Bytes()[:size])
	require.NoError(t, err)
	assert.Equal(t, originalCommit.Message, parsedCommit.Message)
	assert.Equal(t, originalCommit.ObjectID, parsedCommit.ObjectID)
	assert.Equal(t, originalCommit.Parent, parsedCommit.Parent)
	assert.Equal(t, originalCommit.Author.Name, parsedCommit.Author.Name)
	assert.Equal(t, originalCommit.Author.Email, parsedCommit.Author.Email)
	assert.Equal(t, originalCommit.Author.Now.Unix(), parsedCommit.Author.Now.Unix())
}
