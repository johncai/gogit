package commit

import (
	"bytes"
	"crypto/sha1"
	"errors"
	"fmt"
	"strconv"
	"strings"
	"time"

	"gitlab.com/johncai/gogit/repository/types"
)

type Commit struct {
	Parent   types.Oid
	ObjectID types.Oid
	TreeOid  types.Oid
	Author   *Author
	Message  string
}

func New(parent, treeOid types.Oid, author *Author, message string) *Commit {
	c := &Commit{
		TreeOid: treeOid,
		Author:  author,
		Message: message,
		Parent:  parent,
	}

	c.ObjectID = sha1.Sum([]byte(c.String()))
	return c
}

func (c *Commit) Type() string {
	return "commit"
}

func (c *Commit) Oid() types.Oid {
	return c.ObjectID
}

func (c *Commit) String() string {
	lines := []string{
		fmt.Sprintf("tree %s", c.TreeOid),
	}

	if c.Parent != types.EmptyOid {
		lines = append(lines, fmt.Sprintf("parent %s", c.Parent.String()))
	}

	lines = append(lines,
		fmt.Sprintf("author %s", c.Author),
		fmt.Sprintf("comitter %s", c.Author),
		"",
		c.Message,
	)

	content := strings.Join(lines, "\n")

	return fmt.Sprintf("%s %d\x00%s", "commit", len(content), content)
}

// Parse parses a byte slice into a Commit object
func Parse(d []byte) (*Commit, error) {
	headerBody := bytes.Split(d, []byte("\n\n"))
	header := headerBody[0]
	body := headerBody[1]

	lines := bytes.Split(header, []byte("\n"))
	commitHeader := make(map[string]string)

	for _, line := range lines {
		splitLine := bytes.SplitN(line, []byte(" "), 2)
		commitHeader[string(splitLine[0])] = string(splitLine[1])
	}

	var commit Commit

	treeOid, ok := commitHeader["tree"]
	if !ok {
		return nil, errors.New("commit data missing tree sha")
	}

	var err error

	commit.TreeOid, err = types.ParseOid(treeOid)
	if !ok {
		return nil, errors.New("error parsing tree sha")
	}

	parent, ok := commitHeader["parent"]
	if ok {
		if commit.Parent, err = types.ParseOid(parent); err != nil {
			return nil, errors.New("error when parsing parent sha")
		}
	}

	authorString, ok := commitHeader["author"]

	if ok {
		commit.Author = parseAuthor(authorString)
	}

	commit.Message = string(body)

	commit.ObjectID = sha1.Sum([]byte(commit.String()))

	return &commit, nil
}

func (c *Commit) Mode() string {
	return ""
}

const maxUnixCommitDate = 1 << 53

var fallbackTimeValue = time.Unix(1<<63-62135596801, 999999999)

func parseAuthor(line string) *Author {
	var author Author

	splitName := strings.SplitN(line, "<", 2)
	author.Name = strings.TrimSuffix(splitName[0], " ")

	if len(splitName) < 2 {
		return &author
	}

	line = splitName[1]
	splitEmail := strings.SplitN(line, ">", 2)
	if len(splitEmail) < 2 {
		return &author
	}

	author.Email = splitEmail[0]

	secSplit := strings.Fields(splitEmail[1])
	if len(secSplit) < 1 {
		return &author
	}

	sec, err := strconv.ParseInt(secSplit[0], 10, 64)
	if err != nil || sec > maxUnixCommitDate || sec < 0 {
		sec = fallbackTimeValue.Unix()
	}

	author.Now = time.Unix(sec, 0)

	return &author
}

type Author struct {
	Name  string
	Email string
	Now   time.Time
}

func (a *Author) String() string {
	return fmt.Sprintf("%s <%s> %d", a.Name, a.Email, a.Now.Unix())
}

func NewAuthor(name, email string) *Author {
	return &Author{
		Name:  name,
		Email: email,
		Now:   time.Now(),
	}
}
