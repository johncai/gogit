package refs

import (
	"bufio"
	"errors"
	"fmt"
	"io"
	"os"
	"path/filepath"
	"strings"

	"gitlab.com/johncai/gogit/lockfile"
	"gitlab.com/johncai/gogit/repository/types"
)

var (
	LockDeniedErr = errors.New("lock denied")
)

type Refs struct {
	path     string
	lockfile *lockfile.LockFile
}

func New(path string) *Refs {
	return &Refs{path: path, lockfile: lockfile.New(path)}
}

func (r *Refs) UpdateHead(oid string) error {
	if ok, err := r.lockfile.HoldForUpdate(); !ok || err != nil {
		return LockDeniedErr
	}

	r.lockfile.Write(oid)
	r.lockfile.Write("\n")
	if err := r.lockfile.Commit(); err != nil {
		return err
	}

	return nil
}

func (r *Refs) HeadPath() string {
	return filepath.Join(r.path, "HEAD")
}

func (r *Refs) ReadHead() (types.Oid, error) {

	var o types.Oid
	f, err := os.Open(r.HeadPath())
	if os.IsNotExist(err) {
		return o, nil
	}
	if err != nil {
		return o, err
	}
	defer f.Close()
	reader := bufio.NewReader(f)
	s, err := reader.ReadString('\n')
	if err != nil {
		if err == io.EOF {
			return o, nil
		}
		return o, err
	}

	s = strings.TrimRight(s, "\n")

	// try to dereference
	if strings.HasPrefix(s, "ref") {
		refSplit := strings.SplitN(s, "ref: ", 1)
		refFile, err := os.Open(filepath.Join(r.path, refSplit[0]))
		if err != nil {
			if os.IsNotExist(err) {
				return o, nil
			}
			return o, fmt.Errorf("error when dereferencing HEAD: %v", err)
		}
		reader = bufio.NewReader(refFile)
		sha, err := reader.ReadString('\n')
		if err != nil {
			return o, fmt.Errorf("error when reading deferenced HEAD ref: %v", err)
		}

		s = sha[:len(sha)-1]
	}

	o, err = types.ParseOid(s)
	if err != nil {
		return o, err
	}
	return o, nil
}
