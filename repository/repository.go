package repository

import (
	"crypto/sha1"
	"path/filepath"

	"gitlab.com/johncai/gogit/repository/database"
	"gitlab.com/johncai/gogit/repository/index"
	"gitlab.com/johncai/gogit/repository/refs"
	"gitlab.com/johncai/gogit/repository/tree"
	"gitlab.com/johncai/gogit/repository/workspace"
)

type Repository struct {
	rootPath  string
	database  *database.Database
	index     *index.Index
	refs      *refs.Refs
	workspace *workspace.Workspace
}

func New(root string) *Repository {
	return &Repository{
		rootPath:  root,
		database:  database.New(filepath.Join(root, "objects")),
		index:     index.New(filepath.Join(root, "index")),
		refs:      refs.New(root),
		workspace: workspace.New(filepath.Dir(root)),
	}
}

func (r *Repository) Index() *index.Index {
	return r.index
}

func (r *Repository) Database() *database.Database {
	return r.database
}

func (r *Repository) Refs() *refs.Refs {
	return r.refs
}

func (r *Repository) Workspace() *workspace.Workspace {
	return r.workspace
}

func (r *Repository) StoreTree(t *tree.Tree) error {
	for _, entry := range t.Entries() {
		if tree, ok := entry.(*tree.Tree); ok {
			if err := r.StoreTree(tree); err != nil {
				return err
			}
		}
	}
	t.SetOid(sha1.Sum([]byte(t.String())))
	return r.database.Store(t)
}
