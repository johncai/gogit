package database

import (
	"bufio"
	"bytes"
	"compress/zlib"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"path/filepath"
	"strconv"

	"gitlab.com/johncai/gogit/repository/blob"
	"gitlab.com/johncai/gogit/repository/commit"
	"gitlab.com/johncai/gogit/repository/tree"
	"gitlab.com/johncai/gogit/repository/types"
)

type Database struct {
	rootPath string
	objects  map[types.Oid]types.Object
}

func New(path string) *Database {
	return &Database{
		rootPath: path,
		objects:  make(map[types.Oid]types.Object),
	}
}

func (d *Database) Store(o types.Object) error {
	return d.write(o)
}

func (d *Database) Load(o types.Oid) (types.Object, error) {
	if object, ok := d.objects[o]; ok {
		return object, nil
	}

	object, err := d.readObject(o)
	if err != nil {
		return nil, err
	}

	d.objects[o] = object
	return object, nil
}

func (d *Database) readObject(o types.Oid) (types.Object, error) {
	path := d.objectPath(o)

	f, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer f.Close()

	zlibReader, err := zlib.NewReader(f)
	if err != nil {
		return nil, err
	}
	defer zlibReader.Close()

	stringReader := bufio.NewReader(zlibReader)

	b, err := stringReader.ReadBytes(' ')
	if err != nil {
		return nil, err
	}

	objectType := string(b[:len(b)-1])
	// read size
	sizeBytes, err := stringReader.ReadBytes(0x00)
	if err != nil {
		return nil, err
	}

	sizeBytes = sizeBytes[:len(sizeBytes)-1]

	size, err := strconv.ParseInt(string(sizeBytes), 10, 32)
	if err != nil {
		return nil, err
	}

	var data bytes.Buffer

	if _, err = io.Copy(&data, stringReader); err != nil {
		return nil, err
	}

	var object types.Object

	switch objectType {
	case "tree":
		object, err = tree.Parse(data.Bytes()[:size])
	case "commit":
		object, err = commit.Parse(data.Bytes()[:size])
	case "blob":
		object = blob.New(data.Bytes()[:size])
	}

	return object, nil
}

func (d *Database) objectPath(oid types.Oid) string {
	oidString := oid.String()
	return filepath.Join(d.rootPath, oidString[:2], oidString[2:])
}

func (d *Database) write(o types.Object) error {
	path := d.objectPath(o.Oid())

	if _, err := os.Stat(path); !os.IsNotExist(err) {
		return nil
	}

	if err := os.MkdirAll(filepath.Dir(path), os.ModeDir|0755); err != nil {
		return fmt.Errorf("failed to make object path: %v", err)
	}
	tempFile, err := ioutil.TempFile(filepath.Dir(path), "tmp")
	if err != nil {
		return fmt.Errorf("failed to create temp file: %v", err)
	}

	var b bytes.Buffer
	zlibWriter := zlib.NewWriter(&b)
	if _, err = zlibWriter.Write([]byte(o.String())); err != nil {
		return fmt.Errorf("failed to write to zlib writer: %v", err)
	}
	zlibWriter.Close()

	if _, err = io.Copy(tempFile, &b); err != nil {
		return fmt.Errorf("error when writing zlib comrpessed file to temp file: %v", err)
	}
	tempFile.Close()

	if err = os.Rename(tempFile.Name(), path); err != nil {
		return fmt.Errorf("failed to rename the temp file to the real object file: %v", err)
	}

	return nil
}
