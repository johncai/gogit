package types

type Object interface {
	Type() string
	Oid() Oid
	String() string
}
