package types

import (
	"crypto/sha1"
	"encoding/hex"
	"os"
)

type Entry interface {
	os.FileInfo
	//Name() string
	Oid() Oid
	//Mode() string
	String() string
}

type Oid [sha1.Size]byte

func (o Oid) String() string {
	return hex.EncodeToString(o[:sha1.Size])
}

var EmptyOid = Oid{}

func ParseOid(s string) (Oid, error) {
	o, err := hex.DecodeString(s)
	if err != nil {
		return Oid{}, err
	}

	var oid [sha1.Size]byte

	copy(oid[:], o)

	return oid, err
}
