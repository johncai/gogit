package index

import (
	"crypto/sha1"
	"encoding/binary"
	"errors"
	"fmt"
	"hash"
	"io/ioutil"
	"os"
	"sort"
	"strings"
	"syscall"
	"time"

	"gitlab.com/johncai/gogit/lockfile"
	"gitlab.com/johncai/gogit/repository/types"
)

const (
	RegularMode    = 0100644
	ExecutableMode = 0100755
	MaxPathSize    = 0xfff
)

type Index struct {
	entries   []*CacheEntry
	entryMap  map[string]struct{}
	lockfile  *lockfile.LockFile
	indexFile string
	changed   bool
}

func New(path string) *Index {
	lockfile := lockfile.New(path)
	return &Index{
		entries:   make([]*CacheEntry, 0),
		entryMap:  make(map[string]struct{}),
		lockfile:  lockfile,
		indexFile: path,
	}
}

func (i *Index) Entries() []types.Entry {
	var entries []types.Entry

	for _, entry := range i.entries {
		entries = append(entries, entry)
	}

	return entries
}

func readHeader(f *os.File, h hash.Hash) (int, error) {
	header := make([]byte, 12)
	if _, err := f.Read(header); err != nil {
		return 0, err
	}
	signature := string(header[:4])
	version := binary.BigEndian.Uint32(header[4:])
	count := binary.BigEndian.Uint32(header[8:])

	if signature != "DIRC" {
		return 0, errors.New("invalid signature")
	}

	if version != 2 {
		return 0, errors.New("invalid version")
	}
	h.Write(header)
	return int(count), nil
}

// readEntries assumes f has been read up to and including the header, leaving the entries and the checksum
func (i *Index) readEntries(f *os.File, count int, h hash.Hash) error {
	for j := 0; j < count; j++ {
		entry := make([]byte, 64)
		_, err := f.Read(entry)
		if err != nil {
			return err
		}
		for entry[len(entry)-1] != 0x00 {
			block := make([]byte, 8)
			_, err := f.Read(block)
			if err != nil {
				return err
			}
			entry = append(entry, block...)
		}

		h.Write(entry)
		cacheEntry := loadEntry(entry)
		i.entryMap[cacheEntry.Pathname()] = struct{}{}

		i.entries = append(i.entries, cacheEntry)

	}
	return nil
}

func loadEntry(d []byte) *CacheEntry {
	return &CacheEntry{
		data: d,
	}
}

func (i *Index) Clear() {
	i.entries = make([]*CacheEntry, 0)
}

func (i *Index) Load() error {
	if ok, err := i.lockfile.HoldForUpdate(); !ok || err != nil {
		return err
	}
	i.entries = make([]*CacheEntry, 0)

	f, err := os.Open(i.indexFile)
	if err != nil {
		if os.IsNotExist(err) {
			return nil
		}
		return fmt.Errorf("error when loading index file: %v", err)
	}
	defer f.Close()

	hash := sha1.New()

	count, err := readHeader(f, hash)
	if err != nil {
		return err
	}

	if err = i.readEntries(f, count, hash); err != nil {
		return err
	}

	checksum, err := ioutil.ReadAll(f)
	if err != nil {
		return err
	}

	if string(checksum) != string(hash.Sum(nil)) {
		return errors.New("invalid sha")
	}

	return nil
}

func (i *Index) IsTracked(fileName string) bool {
	for k, _ := range i.entryMap {
		filepaths := strings.Split(k, "/")
		for j := 0; j < len(filepaths); j++ {
			if fileName == strings.Join(filepaths[:j+1], "/") {
				return true
			}
		}
	}

	return false

}

func (i *Index) discardConflicts(c *CacheEntry) {
	incomingEntryDirs := strings.Split(c.Pathname(), "/")

	for index, v := range i.entries {
		for j := 0; j < len(incomingEntryDirs); j++ {
			if strings.Join(incomingEntryDirs[0:j], "/") == v.Pathname() {
				i.entries = append(i.entries[:index], i.entries[index+1:]...)
			}
		}

		existingEntryDirs := strings.Split(v.Pathname(), "/")
		for j := 0; j < len(existingEntryDirs); j++ {
			if strings.Join(existingEntryDirs[0:j], "/") == c.Pathname() {
				i.entries = append(i.entries[:index], i.entries[index+1:]...)
			}
		}
	}
}

func (i *Index) Add(pathname string, oid types.Oid, stat os.FileInfo) {
	if _, ok := i.entryMap[pathname]; ok {
		return
	}

	i.entryMap[pathname] = struct{}{}

	c := NewCacheEntry(pathname, oid, stat)

	i.discardConflicts(c)

	i.entries = append(i.entries, c)
}

type byPathname []*CacheEntry

func (s byPathname) Len() int           { return len(s) }
func (s byPathname) Swap(i, j int)      { s[i], s[j] = s[j], s[i] }
func (s byPathname) Less(i, j int) bool { return s[i].Pathname() < s[j].Pathname() }

func (i *Index) Write() error {
	if i.changed {
		return i.lockfile.Rollback()
	}

	data := make([]byte, 12)
	data[0] = 'D'
	data[1] = 'I'
	data[2] = 'R'
	data[3] = 'C'
	data[4] = 0x00
	data[5] = 0x00
	data[6] = 0x00
	data[7] = 0x02

	binary.BigEndian.PutUint32(data[8:], uint32(len(i.entries)))

	sort.Sort(byPathname(i.entries))

	for _, entry := range i.entries {
		data = append(data, []byte(entry.String())...)
	}

	sha := sha1.Sum(data)

	data = append(data, sha[:sha1.Size]...)

	if ok, err := i.lockfile.HoldForUpdate(); !ok || err != nil {
		return err
	}

	i.lockfile.Write(string(data))
	if err := i.lockfile.Commit(); err != nil {
		return errors.New("error when committing lockfile")
	}

	i.changed = false

	return nil
}

type CacheEntry struct {
	data []byte
}

func (c *CacheEntry) CtimeSec() uint32 {
	return binary.BigEndian.Uint32(c.data[0:])
}

func (c *CacheEntry) CtimeNsec() uint32 {
	return binary.BigEndian.Uint32(c.data[4:])
}

func (c *CacheEntry) MtimeSec() uint32 {
	return binary.BigEndian.Uint32(c.data[8:])
}

func (c *CacheEntry) MtimeNsec() uint32 {
	return binary.BigEndian.Uint32(c.data[12:])
}

func (c *CacheEntry) Dev() uint32 {
	return binary.BigEndian.Uint32(c.data[16:])
}

func (c *CacheEntry) Ino() uint32 {
	return binary.BigEndian.Uint32(c.data[20:])
}

func (c *CacheEntry) Mod() uint32 {
	return binary.BigEndian.Uint32(c.data[24:])
}

func (c *CacheEntry) Uid() uint32 {
	return binary.BigEndian.Uint32(c.data[28:])
}

func (c *CacheEntry) Gid() uint32 {
	return binary.BigEndian.Uint32(c.data[32:])
}

func (c *CacheEntry) Oid() types.Oid {
	var s types.Oid
	copy(s[:], c.data[40:60])
	return s
}

func (c *CacheEntry) IsDir() bool {
	return os.ModeDir&c.Mode() > 0
}

func (c *CacheEntry) Sys() interface{} {
	return nil
}

func (c *CacheEntry) Size() int64 {
	return int64(binary.BigEndian.Uint32(c.data[36:]))
}

func (c *CacheEntry) ModTime() time.Time {
	return time.Unix(int64(c.MtimeSec()), int64(c.MtimeNsec()))
}

func (c *CacheEntry) Length() uint16 {
	return binary.BigEndian.Uint16(c.data[60:])
}

func (c *CacheEntry) Pathname() string {
	var pathBytes []byte

	index := 62
	for index < len(c.data) && c.data[index] != 0x00 {
		pathBytes = append(pathBytes, c.data[index])
		index++
	}
	return string(pathBytes)
}

func (c *CacheEntry) Name() string {
	return c.Pathname()
}

func (c *CacheEntry) Mode() os.FileMode {
	return os.FileMode(c.Mod())
	//	return fmt.Sprintf("%o", c.Mod())
}

func (c *CacheEntry) String() string {
	return string(c.data)
}

func NewCacheEntry(pathname string, oid types.Oid, stat os.FileInfo) *CacheEntry {
	s := stat.Sys().(*syscall.Stat_t)

	b := make([]byte, 62)

	binary.BigEndian.PutUint32(b[0:], uint32(s.Ctimespec.Sec))
	binary.BigEndian.PutUint32(b[4:], uint32(s.Ctimespec.Nsec))
	binary.BigEndian.PutUint32(b[8:], uint32(s.Mtimespec.Sec))
	binary.BigEndian.PutUint32(b[12:], uint32(s.Mtimespec.Nsec))
	binary.BigEndian.PutUint32(b[16:], uint32(s.Dev))
	binary.BigEndian.PutUint32(b[20:], uint32(s.Ino))
	binary.BigEndian.PutUint32(b[24:], uint32(s.Mode))
	binary.BigEndian.PutUint32(b[28:], s.Uid)
	binary.BigEndian.PutUint32(b[32:], s.Gid)
	binary.BigEndian.PutUint32(b[36:], uint32(stat.Size()))

	copy(b[40:], oid[:sha1.Size])
	binary.BigEndian.PutUint16(b[60:], uint16(len(pathname)))
	b = append(b, []byte(pathname)...)
	b = append(b, 0x00)

	for len(b)%8 != 0 {
		b = append(b, 0x00)
	}

	return &CacheEntry{
		data: b,
	}
}
