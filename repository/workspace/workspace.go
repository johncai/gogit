package workspace

import (
	"io/ioutil"
	"os"
	"path/filepath"
)

type Workspace struct {
	rootPath string
	gitPath  string
	dbPath   string
}

func New(path string) *Workspace {
	gitPath := filepath.Join(path, ".git")
	dbPath := filepath.Join(gitPath, "objects")

	return &Workspace{
		rootPath: path,
		gitPath:  gitPath,
		dbPath:   dbPath,
	}
}

func (w *Workspace) Gitdir() string {
	return w.gitPath
}

func (w *Workspace) ListDir(prefix string) (map[string]os.FileInfo, error) {
	fis, err := ioutil.ReadDir(filepath.Join(w.rootPath, prefix))
	if err != nil {
		return nil, err
	}
	dirs := make(map[string]os.FileInfo)

	for _, fi := range fis {
		if fi.Name() != ".git" {
			dirs[fi.Name()] = fi
		}
	}

	return dirs, nil
}

func (w *Workspace) HasFiles(path string) bool {
	var foundFile bool
	filepath.Walk(filepath.Join(w.rootPath, path), func(path string, info os.FileInfo, err error) error {
		if !info.IsDir() {
			foundFile = true
		}
		return nil
	})
	return foundFile
}

func (w *Workspace) ListFiles(dir string) ([]string, error) {
	if dir == "" {
		dir = w.rootPath
	}

	var filePaths []string

	fis, err := ioutil.ReadDir(dir)
	if err != nil {
		return nil, err
	}

	for _, fi := range fis {
		if !fi.IsDir() {
			path, err := filepath.Rel(w.rootPath, filepath.Join(dir, fi.Name()))
			if err != nil {
				return nil, err
			}
			filePaths = append(filePaths, path)
			continue
		}
		if fi.Name() == ".git" {
			continue
		}

		filesInDir, err := w.ListFiles(filepath.Join(dir, fi.Name()))
		if err != nil {
			return nil, err
		}
		filePaths = append(filePaths, filesInDir...)
	}

	return filePaths, nil
}

func (w *Workspace) ReadFile(path string) ([]byte, error) {
	f, err := os.Open(filepath.Join(w.rootPath, path))
	if err != nil {
		return nil, err
	}
	defer f.Close()
	b, err := ioutil.ReadAll(f)
	if err != nil {
		return nil, err
	}
	return b, nil
}
