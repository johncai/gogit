package blob

import (
	"crypto/sha1"
	"fmt"

	"gitlab.com/johncai/gogit/repository/types"
)

type Blob struct {
	oid     [sha1.Size]byte
	data    []byte
	content string
}

func New(d []byte) *Blob {
	content := fmt.Sprintf("%s %d\x00%s", "blob", len(d), string(d))
	oid := sha1.Sum([]byte(content))

	return &Blob{data: d, content: content, oid: oid}
}

func (b Blob) Type() string {
	return "blob"
}

func (b *Blob) Oid() types.Oid {
	return b.oid
}

func (b *Blob) String() string {
	return b.content
}
