package errorhandlers

import (
	"fmt"
	"io"
	"os"
)

func ThrowError(w io.Writer, message string, code int) {
	fmt.Fprintln(w, message)
	os.Exit(code)
}
