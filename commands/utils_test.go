package commands_test

import (
	"io/ioutil"
	"os"
	"os/exec"
	"path/filepath"
	"strings"
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/johncai/gogit/commands"
)

func setupGitDir(t *testing.T) (string, func()) {
	dir, err := ioutil.TempDir("", t.Name())
	require.NoError(t, err)

	initCmd := commands.Init()
	require.NoError(t, initCmd.Execute(dir, nil, nil, nil, ioutil.Discard, ioutil.Discard))

	return dir, func() {
		os.RemoveAll(dir)
	}
}

func touchFile(t *testing.T, dir, filename string) *os.File {
	f, err := os.Create(filepath.Join(dir, filename))
	require.NoError(t, err)
	return f
}

func createDir(t *testing.T, dir, dirname string) {
	require.NoError(t, os.MkdirAll(filepath.Join(dir, dirname), os.ModeDir|0755))
}

func changeFileContents(t *testing.T, filepath string, newContents string) {
	f, err := os.OpenFile(filepath, os.O_APPEND|os.O_WRONLY, 0600)
	require.NoError(t, err)
	defer f.Close()
	_, err = f.WriteString(newContents)
	require.NoError(t, err)
}

func changeFileMode(t *testing.T, filepath string, mode os.FileMode) {
	require.NoError(t, os.Chmod(filepath, mode))
}

func replaceFileContents(t *testing.T, filepath string, newContents string) {
	f, err := os.Create(filepath)
	require.NoError(t, err)
	defer f.Close()
	_, err = f.Write([]byte("hello"))
	require.NoError(t, err)
}

func deleteAll(t *testing.T, filepath string) {
	require.NoError(t, os.RemoveAll(filepath))
}

func MustRunCommand(t *testing.T, name string, args ...string) string {
	cmd := exec.Command(name, args...)
	output, err := cmd.Output()
	require.NoError(t, err)

	return strings.TrimRight(string(output), "\n")
}
