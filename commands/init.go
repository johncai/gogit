package commands

import (
	"fmt"
	"io"
	"os"
	"path/filepath"
)

type InitCmd struct {
}

func Init() *InitCmd {
	return &InitCmd{}
}

func (i *InitCmd) Execute(cwd string, env, args []string, stdin io.Reader, stdout, stderr io.Writer) error {
	var relPath string
	if len(args) > 0 {
		relPath = args[0]
	}
	path, err := filepath.Abs(filepath.Join(cwd, relPath, ".git"))
	if err != nil {
		return fmt.Errorf("error when getting path: %v", err)
	}

	if err := os.MkdirAll(path, os.ModeDir|0755); err != nil {
		return fmt.Errorf("failed to create %v: %v", path, err)
	}

	gitDirectories := []string{"objects", "refs"}

	for _, gitDir := range gitDirectories {
		if err := os.Mkdir(filepath.Join(path, gitDir), os.ModeDir|0755); err != nil {
			return fmt.Errorf("failed to create %v: %v", gitDir, err)
		}
	}

	headFile, err := os.Create(filepath.Join(path, "HEAD"))
	if err != nil {
		return fmt.Errorf("error when creating HEAD: %v", err)
	}
	defer headFile.Close()

	if _, err = headFile.WriteString("ref: refs/heads/master\n"); err != nil {
		return fmt.Errorf("error when writing HEAD: %v", err)
	}
	fmt.Fprintf(stdout, "Initialized empty gogit repository in %s\n", path)

	return nil
}
