package commands

import (
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"

	"gitlab.com/johncai/gogit/errorhandlers"
	"gitlab.com/johncai/gogit/repository"
	"gitlab.com/johncai/gogit/repository/blob"
)

type AddCmd struct {
	args []string
}

func Add() *AddCmd {
	return &AddCmd{}
}

func (a *AddCmd) Execute(cwd string, env, args []string, stdin io.Reader, stdout, stderr io.Writer) error {
	r := repository.New(filepath.Join(cwd, ".git"))
	index := r.Index()
	database := r.Database()
	workspace := r.Workspace()

	if err := index.Load(); err != nil {
		errorhandlers.ThrowError(stderr, err.Error(), 128)
	}

	for _, pathArg := range args {
		fullPath := filepath.Join(cwd, pathArg)

		fi, err := os.Stat(fullPath)
		if err != nil {
			errorhandlers.ThrowError(stdout, fmt.Sprintf("pathspec '%s' did not match any files", pathArg), 128)
		}

		var paths []string
		if fi.IsDir() {
			if paths, err = workspace.ListFiles(fullPath); err != nil {
				log.Fatalf("error when getting directory files: %v", err)
			}
		} else {
			paths = []string{pathArg}
		}

		for _, path := range paths {
			f, err := os.Open(filepath.Join(cwd, path))
			if err != nil {
				if os.IsPermission(err) {
					fmt.Fprintf(stdout, "error: open(\"%s\"): Permission denied\n", path)
					fmt.Fprintf(stdout, "error: unable to index file %s\n", path)
					errorhandlers.ThrowError(stdout, "fatal: adding files failed\n", 128)
				}
				errorhandlers.ThrowError(stdout, "fatal: adding files failed\n", 128)
			}
			stat, err := f.Stat()
			if err != nil {
				fmt.Printf("File %v error %v\n\n", f.Name(), err)
				errorhandlers.ThrowError(stderr, err.Error(), 128)
			}
			defer f.Close()
			data, err := ioutil.ReadAll(f)
			b := blob.New(data)
			database.Store(b)

			index.Add(path, b.Oid(), stat)
		}
	}
	if err := index.Write(); err != nil {
		log.Fatalf("error when writing index file: %v", err)
		errorhandlers.ThrowError(stdout, "fatal: adding files failed\n", 128)
	}
	return nil
}
