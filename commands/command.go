package commands

import "io"

type Command interface {
	Execute(dir string, env, args []string, stdin io.Reader, stdout, stderr io.Writer) error
}
