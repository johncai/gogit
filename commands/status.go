package commands

import (
	"errors"
	"fmt"
	"io"
	"os"
	"path/filepath"
	"sort"

	"gitlab.com/johncai/gogit/repository/commit"
	"gitlab.com/johncai/gogit/repository/tree"

	"gitlab.com/johncai/gogit/repository"
	"gitlab.com/johncai/gogit/repository/blob"
	"gitlab.com/johncai/gogit/repository/types"
)

type changeType string

const (
	Modified  changeType = "M"
	Deleted   changeType = "D"
	Untracked changeType = "??"
	Added     changeType = "A"
)

type StatusCmd struct {
	r *repository.Repository
	//	untracked map[string]struct{}
	changed  map[string]changeType
	stats    map[string]os.FileInfo
	headTree map[string]types.Entry
}

func Status(path string) *StatusCmd {
	return &StatusCmd{
		r: repository.New(filepath.Join(path, ".git")),
		//	untracked: make(map[string]struct{}),
		changed:  make(map[string]changeType),
		stats:    make(map[string]os.FileInfo),
		headTree: make(map[string]types.Entry),
	}
}

func (s *StatusCmd) scanWorkspace(prefix string) error {
	dirs, err := s.r.Workspace().ListDir(prefix)
	if err != nil {
		return err
	}

	for dirName, fi := range dirs {
		if s.r.Index().IsTracked(filepath.Join(prefix, dirName)) {
			s.stats[filepath.Join(prefix, dirName)] = fi
			if fi.IsDir() {
				s.scanWorkspace(filepath.Join(prefix, dirName))
			}
			continue
		}
		if fi.IsDir() {
			dirName = dirName + "/"
		}
		path := dirName
		if prefix != "" {
			path = fmt.Sprintf("%s/%s", prefix, dirName)
		}

		//check if path or any of its children are tracked
		if s.r.Workspace().HasFiles(path) {
			s.changed[path] = Untracked
		}
	}

	return nil
}

func (s *StatusCmd) checkIndexEntries() error {
	index := s.r.Index()

	entriesMap := make(map[string]struct{})
	for _, entry := range index.Entries() {
		if err := s.checkAgainstWorkspace(entry); err != nil {
			return fmt.Errorf("error when checking against workspace: %v", err)
		}
		if err := s.checkAgainstHeadTree(entry); err != nil {
			return fmt.Errorf("error when checking against workspace: %v", err)
		}
		entriesMap[entry.Name()] = struct{}{}
	}

	for k := range s.headTree {
		if _, ok := entriesMap[k]; !ok {
			s.changed[k] = Deleted
		}
	}
	return nil
}

func (s *StatusCmd) checkAgainstWorkspace(entry types.Entry) error {
	fi, ok := s.stats[entry.Name()]
	if !ok {
		s.changed[entry.Name()] = Deleted
		return nil
	}
	if !statMatch(fi, entry) {
		s.changed[entry.Name()] = Modified
		return nil
	}

	data, err := s.r.Workspace().ReadFile(entry.Name())
	if err != nil {
		return err
	}
	blob := blob.New(data)
	if blob.Oid() != entry.Oid() {
		s.changed[entry.Name()] = Modified
	}
	return nil
}

func (s *StatusCmd) checkAgainstHeadTree(entry types.Entry) error {
	headEntry, ok := s.headTree[entry.Name()]

	if !ok {
		s.changed[entry.Name()] = Added
		return nil
	}

	if headEntry.Mode() != entry.Mode() ||
		headEntry.Oid() != entry.Oid() {
		s.changed[entry.Name()] = Modified
	}

	return nil
}

func (s *StatusCmd) detectWorkspaceChanges() error {
	index := s.r.Index()
	for _, entry := range index.Entries() {
		if err := s.checkAgainstWorkspace(entry); err != nil {
			return fmt.Errorf("error when checking against workspace: %v", err)
		}
	}
	return nil
}

func statMatch(fileInfo, entry os.FileInfo) bool {
	return fileInfo.Size() == entry.Size() &&
		fileInfo.ModTime().Equal(entry.ModTime()) &&
		fileInfo.Mode().Perm() == entry.Mode().Perm()
}

func (s *StatusCmd) loadHeadTree() error {
	headOid, err := s.r.Refs().ReadHead()
	if err != nil {
		return fmt.Errorf("error when reading head: %v", err)
	}

	if headOid == types.EmptyOid {
		return nil
	}

	c, err := s.r.Database().Load(headOid)
	if err != nil {
		return fmt.Errorf("error when loading head commit: %v", err)
	}

	commit, ok := c.(*commit.Commit)
	if !ok {
		return errors.New("not a commit")
	}

	if err = s.readTree(commit.TreeOid); err != nil {
		return fmt.Errorf("error when reading tree: %v", err)
	}

	return nil
}

func (s *StatusCmd) readTree(treeOid types.Oid) error {
	o, err := s.r.Database().Load(treeOid)
	if err != nil {
		return err
	}

	t, ok := o.(*tree.Tree)
	if !ok {
		return errors.New("oid must be of type tree")
	}

	for _, entry := range t.Entries() {
		if entry.Mode() == os.FileMode(040000) {
			s.readTree(entry.Oid())
		} else {
			s.headTree[entry.Name()] = entry
		}
	}

	return nil
}

func (s *StatusCmd) Execute(cwd string, env, args []string, stdin io.Reader, stdout, stderr io.Writer) error {
	index := s.r.Index()

	if err := index.Load(); err != nil {
		return fmt.Errorf("error when loading index: %v", err)
	}

	if err := s.scanWorkspace(""); err != nil {
		return fmt.Errorf("error when scanning workspace: %v", err)
	}

	if err := s.loadHeadTree(); err != nil {
		return fmt.Errorf("error when loading head tree: %v", err)
	}

	if err := s.checkIndexEntries(); err != nil {
		return fmt.Errorf("error when detecting workspace changes: %v", err)
	}

	keys := make([]string, 0, len(s.changed))

	for k := range s.changed {
		keys = append(keys, k)
	}

	sort.Strings(keys)

	for _, k := range keys {
		t, _ := s.changed[k]
		fmt.Fprintf(stdout, " %s %s\n", t, k)
	}

	return nil
}
