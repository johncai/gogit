package commands

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"path/filepath"

	"gitlab.com/johncai/gogit/errorhandlers"
	"gitlab.com/johncai/gogit/repository"
	"gitlab.com/johncai/gogit/repository/commit"
	"gitlab.com/johncai/gogit/repository/tree"
)

type CommitCmd struct {
	args []string
}

func Commit() *CommitCmd {
	return &CommitCmd{}
}

func (cc *CommitCmd) Execute(cwd string, env, args []string, stdin io.Reader, stdout, stderr io.Writer) error {
	r := repository.New(filepath.Join(cwd, ".git"))
	workspace := r.Workspace()
	index := r.Index()
	database := r.Database()
	refs := r.Refs()

	if err := index.Load(); err != nil {
		return fmt.Errorf("error when loading index: %v", err)
	}

	t, err := tree.Build(index.Entries())
	if err != nil {
		return fmt.Errorf("error when building tree: %v", err)
	}
	if err := r.StoreTree(t); err != nil {
		return fmt.Errorf("error when storing tree: %v", err)
	}

	name, email := os.Getenv("GIT_AUTHOR_NAME"), os.Getenv("GIT_AUTHOR_EMAIL")
	author := commit.NewAuthor(name, email)
	reader := bufio.NewReader(stdin)
	message, err := reader.ReadString('\n')
	if err != nil {
		return fmt.Errorf("error when reading stdin: %v", err)
	}

	parentOid, err := refs.ReadHead()
	if err != nil {
		return fmt.Errorf("error reading head: %v", err)
	}

	c := commit.New(parentOid, t.Oid(), author, message)
	database.Store(c)

	head, err := os.Create(filepath.Join(workspace.Gitdir(), "HEAD"))
	if err != nil {
		return fmt.Errorf("error when opening HEAD: %v", err)
	}

	_, err = head.WriteString(fmt.Sprintf("%s\n", c.Oid().String()))
	if err != nil {
		return fmt.Errorf("error when writing to HEAD: %v", err)
	}

	if err = index.Write(); err != nil {
		errorhandlers.ThrowError(stderr, err.Error(), 128)
	}

	index.Clear()
	fmt.Fprintf(stdout, "[(root-commit) %s] %s", c.Oid(), message)

	return nil
}
