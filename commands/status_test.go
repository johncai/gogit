package commands_test

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/johncai/gogit/commands"
)

func TestStatus_UntrackedFiles(t *testing.T) {
	gitDir, cleanup := setupGitDir(t)
	defer cleanup()
	newFiles := []string{"file1", "file2", "file3"}

	for _, newFile := range newFiles {
		touchFile(t, gitDir, newFile)
	}

	var stdout bytes.Buffer

	statusCmd := commands.Status(gitDir)
	require.NoError(t, statusCmd.Execute(gitDir, nil, nil, nil, &stdout, nil))

	outputLines := strings.Split(strings.TrimRight(stdout.String(), "\n"), "\n")

	for i, line := range outputLines {
		assert.Equal(t, fmt.Sprintf(" ?? %s", newFiles[i]), line)
	}
}
func TestStatus_FileInIndex(t *testing.T) {
	gitDir, cleanup := setupGitDir(t)
	defer cleanup()
	newFile := "file1"

	touchFile(t, gitDir, newFile)

	addCmd := commands.Add()
	require.NoError(t, addCmd.Execute(gitDir, nil, []string{newFile}, nil, ioutil.Discard, ioutil.Discard))
	commitCmd := commands.Commit()

	var stdin, stdout bytes.Buffer
	stdin.WriteString("commit message\n")
	require.NoError(t, commitCmd.Execute(gitDir, nil, nil, &stdin, ioutil.Discard, ioutil.Discard))

	statusCmd := commands.Status(gitDir)
	require.NoError(t, statusCmd.Execute(gitDir, nil, nil, nil, &stdout, ioutil.Discard))

	require.Empty(t, strings.TrimRight(stdout.String(), "\n"))
}

func TestStatus_NestedDirectories(t *testing.T) {
	gitDir, cleanup := setupGitDir(t)
	defer cleanup()

	createDir(t, gitDir, "a/b")
	touchFile(t, filepath.Join(gitDir, "a/b"), "inner")

	addCmd := commands.Add()
	require.NoError(t, addCmd.Execute(gitDir, nil, []string{"a/b/"}, nil, ioutil.Discard, ioutil.Discard))
	commitCmd := commands.Commit()

	var stdin, stdout bytes.Buffer
	stdin.WriteString("commit message\n")
	require.NoError(t, commitCmd.Execute(gitDir, nil, nil, &stdin, ioutil.Discard, ioutil.Discard))

	createDir(t, gitDir, "a/b/c")
	touchFile(t, filepath.Join(gitDir, "a"), "outerfile")
	touchFile(t, filepath.Join(gitDir, "a/b/c"), "file.txt")

	statusCmd := commands.Status(gitDir)
	require.NoError(t, statusCmd.Execute(gitDir, nil, nil, nil, &stdout, ioutil.Discard))

	results := strings.Split(strings.TrimRight(stdout.String(), "\n"), "\n")
	assert.Len(t, results, 2)
	assert.Equal(t, " ?? a/b/c/", results[0])
	assert.Equal(t, " ?? a/outerfile", results[1])
}

func TestStatus_EmptyDirectories(t *testing.T) {
	gitDir, cleanup := setupGitDir(t)
	defer cleanup()

	createDir(t, gitDir, "a/b")

	statusCmd := commands.Status(gitDir)

	var stdout bytes.Buffer
	require.NoError(t, statusCmd.Execute(gitDir, nil, nil, nil, &stdout, ioutil.Discard))

	assert.Empty(t, strings.TrimRight(stdout.String(), "\n"))

	stdout.Reset()
	touchFile(t, filepath.Join(gitDir, "a/b"), "file.txt")
	require.NoError(t, statusCmd.Execute(gitDir, nil, nil, nil, &stdout, ioutil.Discard))

	results := strings.Split(strings.TrimRight(stdout.String(), "\n"), "\n")
	assert.Len(t, results, 1)
	assert.Equal(t, " ?? a/", results[0])
}

func setupIndex(t *testing.T, gitDir string) {
	createDir(t, gitDir, "a/b")
	files := []struct {
		dir      string
		name     string
		contents string
	}{
		{
			dir:      gitDir,
			name:     "1.txt",
			contents: "one",
		},
		{
			dir:      filepath.Join(gitDir, "a"),
			name:     "2.txt",
			contents: "two",
		},
		{
			dir:      filepath.Join(gitDir, "a/b"),
			name:     "3.txt",
			contents: "three",
		},
	}

	for _, file := range files {
		f := touchFile(t, file.dir, file.name)
		_, err := f.WriteString(file.contents)
		require.NoError(t, err)
		require.NoError(t, f.Close())
	}

	addCmd := commands.Add()
	require.NoError(t, addCmd.Execute(gitDir, nil, []string{"."}, nil, os.Stderr, os.Stderr))

	commitCmd := commands.Commit()
	var stdin bytes.Buffer
	stdin.WriteString("setting up the index\n")
	require.NoError(t, commitCmd.Execute(gitDir, nil, nil, &stdin, ioutil.Discard, ioutil.Discard))
}

func TestStatus_ChangedContents(t *testing.T) {
	gitDir, cleanup := setupGitDir(t)
	defer cleanup()

	setupIndex(t, gitDir)
	changeFileContents(t, filepath.Join(gitDir, "1.txt"), "changed")
	changeFileContents(t, filepath.Join(gitDir, "a/2.txt"), "modified")

	statusCmd := commands.Status(gitDir)

	var stdout bytes.Buffer
	require.NoError(t, statusCmd.Execute(gitDir, nil, nil, nil, &stdout, ioutil.Discard))

	results := strings.Split(strings.TrimRight(stdout.String(), "\n"), "\n")
	require.Len(t, results, 2)
	assert.Equal(t, " M 1.txt", results[0])
	assert.Equal(t, " M a/2.txt", results[1])
}

func TestStatus_ChangedMode(t *testing.T) {
	gitDir, cleanup := setupGitDir(t)
	defer cleanup()

	setupIndex(t, gitDir)
	changeFileMode(t, filepath.Join(gitDir, "a/2.txt"), 0000)

	statusCmd := commands.Status(gitDir)

	var stdout bytes.Buffer
	require.NoError(t, statusCmd.Execute(gitDir, nil, nil, nil, &stdout, ioutil.Discard))

	results := strings.Split(strings.TrimRight(stdout.String(), "\n"), "\n")
	require.Len(t, results, 1)
	assert.Equal(t, " M a/2.txt", results[0])
}

func TestStatus_SizePreservingChange(t *testing.T) {
	gitDir, cleanup := setupGitDir(t)
	defer cleanup()

	setupIndex(t, gitDir)

	// from "three" to "hello"
	replaceFileContents(t, filepath.Join(gitDir, "a/b/3.txt"), "hello")

	statusCmd := commands.Status(gitDir)

	var stdout bytes.Buffer
	require.NoError(t, statusCmd.Execute(gitDir, nil, nil, nil, &stdout, ioutil.Discard))

	results := strings.Split(strings.TrimRight(stdout.String(), "\n"), "\n")
	require.Len(t, results, 1)
	assert.Equal(t, " M a/b/3.txt", results[0])
}

func TestStatus_ChangeModifiedTime(t *testing.T) {
	gitDir, cleanup := setupGitDir(t)
	defer cleanup()

	setupIndex(t, gitDir)

	// from "three" to "hello"
	replaceFileContents(t, filepath.Join(gitDir, "a/b/3.txt"), "hello")

	MustRunCommand(t, "touch", filepath.Join(gitDir, "a/b/3.txt"))

	statusCmd := commands.Status(gitDir)

	var stdout bytes.Buffer
	require.NoError(t, statusCmd.Execute(gitDir, nil, nil, nil, &stdout, ioutil.Discard))

	results := strings.Split(strings.TrimRight(stdout.String(), "\n"), "\n")
	require.Len(t, results, 1)
	assert.Equal(t, " M a/b/3.txt", results[0])
}

func TestStatus_DeletedFile(t *testing.T) {
	gitDir, cleanup := setupGitDir(t)
	defer cleanup()

	setupIndex(t, gitDir)

	require.NoError(t, os.Remove(filepath.Join(gitDir, "a/b/3.txt")))

	statusCmd := commands.Status(gitDir)

	var stdout bytes.Buffer
	require.NoError(t, statusCmd.Execute(gitDir, nil, nil, nil, &stdout, ioutil.Discard))

	results := strings.Split(strings.TrimRight(stdout.String(), "\n"), "\n")
	require.Len(t, results, 1)
	assert.Equal(t, " D a/b/3.txt", results[0])
}

func TestStatus_DeletedDirectory(t *testing.T) {
	gitDir, cleanup := setupGitDir(t)
	defer cleanup()

	setupIndex(t, gitDir)

	require.NoError(t, os.RemoveAll(filepath.Join(gitDir, "a")))

	statusCmd := commands.Status(gitDir)

	var stdout bytes.Buffer
	require.NoError(t, statusCmd.Execute(gitDir, nil, nil, nil, &stdout, ioutil.Discard))

	results := strings.Split(strings.TrimRight(stdout.String(), "\n"), "\n")
	require.Len(t, results, 2)
	assert.Equal(t, " D a/2.txt", results[0])
	assert.Equal(t, " D a/b/3.txt", results[1])
}

func TestStatus_HeadIndex_Added(t *testing.T) {
	gitDir, cleanup := setupGitDir(t)
	defer cleanup()

	setupIndex(t, gitDir)

	touchFile(t, filepath.Join(gitDir, "a"), "4.txt")

	MustRunCommand(t, "git", "-C", gitDir, "add", ".")

	statusCmd := commands.Status(gitDir)

	var stdout bytes.Buffer
	require.NoError(t, statusCmd.Execute(gitDir, nil, nil, nil, &stdout, ioutil.Discard))

	results := strings.Split(strings.TrimRight(stdout.String(), "\n"), "\n")
	require.Len(t, results, 1)
	assert.Equal(t, " A a/4.txt", results[0])
}

func TestStatus_HeadIndex_ModifyMode(t *testing.T) {
	gitDir, cleanup := setupGitDir(t)
	defer cleanup()

	setupIndex(t, gitDir)

	changeFileMode(t, filepath.Join(gitDir, "a/2.txt"), 0755)

	MustRunCommand(t, "git", "-C", gitDir, "add", ".")

	statusCmd := commands.Status(gitDir)

	var stdout bytes.Buffer
	require.NoError(t, statusCmd.Execute(gitDir, nil, nil, nil, &stdout, ioutil.Discard))

	results := strings.Split(strings.TrimRight(stdout.String(), "\n"), "\n")
	require.Len(t, results, 1)
	assert.Equal(t, " M a/2.txt", results[0])
}

func TestStatus_HeadIndex_ModifyContents(t *testing.T) {
	gitDir, cleanup := setupGitDir(t)
	defer cleanup()

	setupIndex(t, gitDir)

	changeFileContents(t, filepath.Join(gitDir, "a/2.txt"), "changed for the better")

	MustRunCommand(t, "git", "-C", gitDir, "add", ".")

	statusCmd := commands.Status(gitDir)

	var stdout bytes.Buffer
	require.NoError(t, statusCmd.Execute(gitDir, nil, nil, nil, &stdout, ioutil.Discard))

	results := strings.Split(strings.TrimRight(stdout.String(), "\n"), "\n")
	require.Len(t, results, 1)
	assert.Equal(t, " M a/2.txt", results[0])
}

func TestStatus_HeadIndex_DeletedFile(t *testing.T) {
	gitDir, cleanup := setupGitDir(t)
	defer cleanup()

	setupIndex(t, gitDir)

	deleteAll(t, filepath.Join(gitDir, "1.txt"))
	deleteAll(t, filepath.Join(gitDir, ".git", "index"))

	MustRunCommand(t, "git", "-C", gitDir, "add", ".")

	statusCmd := commands.Status(gitDir)

	var stdout bytes.Buffer
	require.NoError(t, statusCmd.Execute(gitDir, nil, nil, nil, &stdout, ioutil.Discard))

	results := strings.Split(strings.TrimRight(stdout.String(), "\n"), "\n")
	require.Len(t, results, 1)
	assert.Equal(t, " D 1.txt", results[0])
}

func TestStatus_HeadIndex_DeletedDirectory(t *testing.T) {
	gitDir, cleanup := setupGitDir(t)
	defer cleanup()

	setupIndex(t, gitDir)

	deleteAll(t, filepath.Join(gitDir, "a"))
	deleteAll(t, filepath.Join(gitDir, ".git", "index"))

	MustRunCommand(t, "git", "-C", gitDir, "add", ".")

	statusCmd := commands.Status(gitDir)

	var stdout bytes.Buffer
	require.NoError(t, statusCmd.Execute(gitDir, nil, nil, nil, &stdout, ioutil.Discard))

	results := strings.Split(strings.TrimRight(stdout.String(), "\n"), "\n")
	require.Len(t, results, 2)
	assert.Equal(t, " D a/2.txt", results[0])
	assert.Equal(t, " D a/b/3.txt", results[1])
}
