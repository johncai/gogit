package commands_test

import (
	"os"
	"path/filepath"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/johncai/gogit/commands"
)

func TestAddOneTimeSuccess(t *testing.T) {
	gitDir, cleanup := setupGitDir(t)
	defer cleanup()
	newFiles := []string{"file1", "file2", "file3"}

	for _, newFile := range newFiles {
		touchFile(t, gitDir, newFile)
	}

	addCmd := commands.Add()
	require.NoError(t, addCmd.Execute(gitDir, nil, newFiles, nil, nil, nil))

	filesInIndex := strings.Split(MustRunCommand(t, "git", "-C", gitDir, "ls-files"), "\n")
	assert.Equal(t, newFiles, filesInIndex)
}

func TestAddMultipleTimesSuccess(t *testing.T) {
	gitDir, cleanup := setupGitDir(t)
	defer cleanup()
	newFiles := []string{"file1", "file2", "file3"}

	addCmd := commands.Add()
	for _, newFile := range newFiles {
		touchFile(t, gitDir, newFile)
		require.NoError(t, addCmd.Execute(gitDir, nil, []string{newFile}, nil, nil, nil))
	}

	filesInIndex := strings.Split(MustRunCommand(t, "git", "-C", gitDir, "ls-files"), "\n")
	assert.Equal(t, newFiles, filesInIndex)
}

func TestAddDirectorySuccess(t *testing.T) {
	gitDir, cleanup := setupGitDir(t)
	defer cleanup()
	newFiles := []string{"file1", "file2", "file3"}
	newDirectories := []string{"dir1", "dir2", "dir3"}

	var expected []string

	addCmd := commands.Add()
	for _, newDir := range newDirectories {
		require.NoError(t, os.MkdirAll(filepath.Join(gitDir, newDir), os.ModeDir|0744))
		for _, newFile := range newFiles {
			touchFile(t, filepath.Join(gitDir, newDir), newFile)
			expected = append(expected, filepath.Join(newDir, newFile))
		}
	}

	require.NoError(t, addCmd.Execute(gitDir, nil, newDirectories, nil, nil, nil))

	filesInIndex := strings.Split(MustRunCommand(t, "git", "-C", gitDir, "ls-files"), "\n")
	assert.Equal(t, expected, filesInIndex)
}

func TestAddSameFilenameAsDir(t *testing.T) {
	gitDir, cleanup := setupGitDir(t)
	defer cleanup()
	name := "file1"
	createDir(t, gitDir, name)
	touchFile(t, filepath.Join(gitDir, name), "emptyfile")

	addCmd := commands.Add()
	require.NoError(t, addCmd.Execute(gitDir, nil, []string{name}, nil, nil, nil))

	filesInIndex := strings.Split(MustRunCommand(t, "git", "-C", gitDir, "ls-files"), "\n")
	require.Len(t, filesInIndex, 1)
	require.Contains(t, filesInIndex[0], name)
	require.NoError(t, os.RemoveAll(filepath.Join(gitDir, name)))

	touchFile(t, gitDir, name)
	require.NoError(t, addCmd.Execute(gitDir, nil, []string{name}, nil, nil, nil))

	filesInIndex = strings.Split(MustRunCommand(t, "git", "-C", gitDir, "ls-files"), "\n")
	require.Len(t, filesInIndex, 1)
	require.Equal(t, name, filesInIndex[0])
}

func TestAddSameDirAsFilename(t *testing.T) {
	gitDir, cleanup := setupGitDir(t)
	defer cleanup()
	name := "file1"
	touchFile(t, gitDir, name)

	addCmd := commands.Add()
	require.NoError(t, addCmd.Execute(gitDir, nil, []string{name}, nil, nil, nil))

	filesInIndex := strings.Split(MustRunCommand(t, "git", "-C", gitDir, "ls-files"), "\n")
	require.Len(t, filesInIndex, 1)
	require.Equal(t, name, filesInIndex[0])

	require.NoError(t, os.Remove(filepath.Join(gitDir, name)))

	createDir(t, gitDir, name)
	touchFile(t, filepath.Join(gitDir, name), "emptyfile")

	require.NoError(t, addCmd.Execute(gitDir, nil, []string{name}, nil, nil, nil))

	filesInIndex = strings.Split(MustRunCommand(t, "git", "-C", gitDir, "ls-files"), "\n")
	require.Len(t, filesInIndex, 1)
	require.Contains(t, filesInIndex[0], name)
}
