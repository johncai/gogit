package main

import (
	"log"
	"os"

	"gitlab.com/johncai/gogit/commands"
)

func main() {
	if len(os.Args) < 2 {
		log.Fatal("command required")
	}

	command := os.Args[1]

	var cmd commands.Command

	switch command {
	case "init":
		cmd = commands.Init()
	case "commit":
		cmd = commands.Commit()
	case "add":
		cmd = commands.Add()
	default:
		log.Fatalf("'%s' is not a gogit command", command)
	}

	cwd, err := os.Getwd()
	if err != nil {
		log.Fatalf("error getting current working directory: %v", err)
	}
	if err := cmd.Execute(
		cwd,
		os.Environ(),
		os.Args[2:],
		os.Stdin,
		os.Stdout,
		os.Stderr); err != nil {
		log.Fatal(err)
	}
}
