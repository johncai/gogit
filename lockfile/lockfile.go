package lockfile

import (
	"errors"
	"fmt"
	"os"
)

var (
	MissingParentError = errors.New("missing parent")
	NoPermissionError  = errors.New("no permission")
	StaleLockError     = errors.New("stale lock")
)

type LockFile struct {
	path string
	lock *os.File
}

func New(path string) *LockFile {
	return &LockFile{
		path: path,
	}
}

func (n LockFile) LockPath() string {
	return fmt.Sprintf("%s.lock", n.path)
}

func (n *LockFile) HoldForUpdate() (bool, error) {
	var err error
	if n.lock != nil {
		return true, nil
	}

	n.lock, err = os.OpenFile(n.LockPath(), os.O_CREATE|os.O_EXCL|os.O_RDWR, 0755)
	if os.IsExist(err) {
		err = fmt.Errorf("Unable to create '%v': File exists.", n.LockPath())
	}
	if err != nil {
		return false, err
	}
	return true, nil
}

func (n *LockFile) Write(s string) error {
	if n.lock == nil {
		return StaleLockError
	}
	_, err := n.lock.WriteString(s)
	if err != nil {
		return fmt.Errorf("error when writing to lockfile: %v", err)
	}

	return nil
}

func (n *LockFile) Commit() error {
	if n.lock == nil {
		return StaleLockError
	}

	if err := n.lock.Close(); err != nil {
		return err
	}

	if err := os.Rename(n.LockPath(), n.path); err != nil {
		return err
	}

	n.lock = nil

	return nil
}

func (n *LockFile) Rollback() error {
	if n.lock == nil {
		return StaleLockError
	}

	n.lock.Close()
	os.Remove(n.LockPath())

	return nil
}
